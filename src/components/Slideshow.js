import React , {Fragment} from 'react';
import { Fade} from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'

const propiedades = {
    duration: 1000,
    transitionDuration : 1000,
    infinite : true,
    indicators : false,
    arrows : false
};

const Slideshow = () => {

    return ( 
        <Fragment>
        <div className="slide-container">
        
        <Fade {...propiedades}>
        
        <div className="each-fade">          
            <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/banner/01_Nuevo_Home_06.webp" alt=""
            className="img"/>
            <div className="d1_">
                <p id="p1">¿Buscas el datazo de </p>
                <p id="p2">la DJ?</p>
                <hr/>
                <p id="p3">¡No busques más, ya estas en Ego!</p>
            </div>
        </div>
        <div className="each-fade">
            <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/banner/01_Nuevo_Home_03.webp" alt=""
            className="img"/>
            <div className="d1_">
                <p id="p1">¿Buscas el datazo de </p>
                <p id="p2">el psicólogo?</p>
                <hr/>
                <p id="p3">¡No busques más, ya estas en Ego!</p>
            </div>
        </div>
        <div className="each-fade">          
            <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/banner/01_Nuevo_Home-04.png" alt=""
            className="img"/>
            <div className="d1_">
                <p id="p1">¿Buscas el datazo de </p>
                <p id="p2">la dentista?</p>
                <hr/>
                <p id="p3">¡No busques más, ya estas en Ego!</p>
            </div>
        </div>
        <div className="each-fade">          
            <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/banner/01_Nuevo_Home-02.png" alt=""
            className="img"/>
            <div className="d1_">
                <p id="p1">¿Buscas el datazo de </p>
                <p id="p2">la niñera?</p>
                <hr/>
                <p id="p3">¡No busques más, ya estas en Ego!</p>
            </div>
        </div>
        <div className="each-fade">          
            <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/banner/01_Nuevo_Home-05.png" alt=""
            className="img"/>
            <div className="d1_">
                <p id="p1">¿Buscas el datazo de </p>
                <p id="p2">la profesora?</p>
                <hr/>
                <p id="p3">¡No busques más, ya estas en Ego!</p>
            </div>
        </div>
        <div className="each-fade">          
            <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/banner/01_Nuevo_Home_Mesa%2Bde%2Btrabajo%2B1.png" alt=""
            className="img"/>
            <div className="d1_">
                <p id="p1">¿Buscas el datazo de </p>
                <p id="p2">el gasfitero?</p>
                <hr/>
                <p id="p3">¡No busques más, ya estas en Ego!</p>
            </div>
        </div>
        </Fade>
        </div>  
        </Fragment>
      );
}
  
 export default Slideshow;