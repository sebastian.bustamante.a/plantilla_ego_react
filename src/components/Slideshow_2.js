import React , {Fragment} from 'react';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'

const propiedades = {
    duration: 100000,
    transitionDuration : 1,
    infinite : true,
    indicators : true,
    arrows : true
};


const Slideshow_2 = () => {

    return ( 
        <Fragment>
        <div className="slide-container_2" >
        
        <Slide {...propiedades}>
        
        <div className="each-fade">          
            <div class="cont">
                <div id="clas1">¿Por qué existimos?</div>
                <div id="clas2">Ego surgió por la necesidad de encontrar diversos y buenos servicios en 
                un solo lugar, servicios que alivian el estrés en nuestras vidas, pero tambien
                nació para dar la tranquilidad de tomar un servicio como si un amigo te lo hubiera
                recomendado </div>
            </div>
        </div>
        <div className="each-fade">
            <div class="cont" id="cont_1">
                <div id="clas1">¿Que hacemos?</div>
                <div id="clas2">Conectamos personas con extraordinarios emprendedores</div>
            </div>  
        </div>
        <div className="each-fade">
            <div class="cont" id="cont_2">
                <div id="clas1">¿Nuestra meta?</div>
                <div id="clas2">Ser la comunidad más grande de servicios del Perú con miras a empoderar
                y dar comodidad a la vida del chambero</div>
            </div>     
        </div>
        
        
        </Slide>
        </div>  
        </Fragment>
      );
}
  
 export default Slideshow_2;