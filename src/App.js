import React from 'react';
import Slideshow from './components/Slideshow'
import Slideshow_2 from './components/Slideshow_2'

function App()  {
  return (

    <div className="contenedor">
      <header >
            <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/Logo_Final-01.png" alt=""/>
            <div>
                <div id="local">
                    <i class="fa fa-caret-down" style={{width: "12px"}}></i>
                    <input type="text" placeholder="Busca tu localidad"/></div>
                <div id="serv">
                    <input type="text" placeholder="Encuentra tu servicio aqui"/>
                    <i class="fa fa-search"></i></div>
                <button id="b1">Publica</button>
                <select id="cat">
                    <option id="cate">Categorias</option>
                    <option>Reparaciones</option>
                    <option>Profesionales</option>
                    <option>Servicios Medicos</option>
                    <option>Otros servicios</option>
                    <option>Hogar</option>
                    <option>Enseñanza</option>
                    <option>Belleza y Fitness</option>
                    <option>Transporte General</option>
                    <option>Recreacion y Ocio</option>
                    <option>Fiestas y Eventos</option>
                </select>
                <button id="b2">Ingresar</button>
                <button id="b3">Registrate</button>
            </div>
      </header>
      <div class="d1">
            <Slideshow/>
      </div>
      <div class="d2">
            <p id="p1"><b>¿Como usar Ego?</b></p>
            <div class="div">
                <div class="sub_div">
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/comoo+usaar/01_Home-04.png" alt=""/>
                    <p>Entre a ego.pe</p>
                </div>
                <div class="sub_div">
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/comoo+usaar/01_Home-05.png" alt=""/>
                    <p>Busca el servicio que quieres</p>
                </div>
                <div class="sub_div">
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/comoo+usaar/01_Home-06.png" alt=""/>
                    <p>Elige a tu chambero(no olvides revisar las reseñas)</p>
                </div>
                <div class="sub_div" id="sub_div_4">
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/comoo+usaar/01_Home-07.png" alt=""/>
                    <p>Ubica el botón "Contáctate conmigo" para comunicarte con tu chambero</p>
                </div>
            </div>
        </div>
        <div class="d3">
            <div class="d3_"><div class="div">
                <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/Icono_Mesa+de+trabajo+1.png" alt=""/>
                <p id="p1">Ahorra tiempo</p>
                <p id="p2">Encuentra tus servicios aquí</p>
            </div>
            <div class="div">
                <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/Icono-02.png" alt=""/>
                <p id="p1">Confia</p>
                <p id="p2">Mira reseñas y recoge</p>
            </div>
            <div class="div">
                <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/Icono-03.png" alt=""/>
                <p id="p1">Sé parte de la elite</p>
                <p id="p2">Valora un servicio en tu comunidad</p>
            </div> </div>
        </div>  
        <main>
            <h1>Nuestros productos</h1>
            <div class="di">
            <div class="div">
                <a href="descripcion_1.html" class="a_img">
                    <img src="https://ego11.s3.amazonaws.com/products-2020-07/products-2020-07/272727_440313.jpg" alt=""/>
                </a>
                <a href="descripcion_1.html"> <p>Servicio Profesional Contable</p> </a>
                <span class="span">Falcorp Peru SAC.</span>
                <div>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-14.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-14.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-14.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-14.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-14.png" alt=""/>
                    <span>5,0</span>
                    <p>1 reseñas</p>
                </div>
                <p>S/. 150 por hora</p>
                <div class="header">
                    <a href="descripcion_1.html"> Más información</a>
                    <button><i class="fa fa-whatsapp"></i> Pedir servicio</button>
                </div>
            </div>
            <div class="div">
                <a href="descripcion_2.html" class="a_img">
                    <img src="https://ego11.s3.amazonaws.com/products-2020-07/MARTINEZRIMAC_5-715040.jpg" alt=""/>
                </a>
                <a href="descripcion_2.html">
                    <p>Contador</p>
                </a>
                <span class="span">Alan Martinez</span>
                <div>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <span>0</span>
                    <p>0 reseñas</p>
                </div>
                <p>S/. 1 por hora</p>
                <div class="header">
                    <a href="descripcion_2.html">Más información</a>
                    <button><i class="fa fa-whatsapp"></i> Pedir servicio</button>
                </div>
            </div>
            <div class="div">
                <a href="descripcion_3.html" class="a_img">
                    <img id="img_3" src="https://ego11.s3.amazonaws.com/products-2020-06/images_8-29222544.jpeg" alt=""/>
                </a>
                <a href="descripcion_3.html">
                    <p>Seguridad Industrial guantes cuero</p>
                </a>
                <span class="span">Carlos Chancafe Garcia</span>
                <div>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <span>0</span>
                    <p>0 reseñas</p>
                </div>
                <p>S/. 6 por hora</p>
                <div class="header">
                    <a href="descripcion_3.html">Más información</a>
                    <button><i class="fa fa-whatsapp"></i> Pedir servicio</button>
                </div>
            </div>
            <div class="div">
                <a href="descripcion_4.html" class="a_img">
                    <img id="img_4" src="https://ego11.s3.amazonaws.com/products-2020-06/Spot_02-2902937.8_ROLF" alt=""/>
                </a>
                <a href="descripcion_4.html">
                    <p style={{margin: "0 0 21.5px 0"}}>Asesoria Empresarial</p>
                </a>
                <span class="span" >Rolf Sac</span>
                <div>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <span>0</span>
                    <p>0 reseñas</p>
                </div>
                <p>S/. 1 por hora</p>
                <div class="header">
                    <a href="descripcion_4.html">Más información</a>
                    <button><i class="fa fa-whatsapp"></i> Pedir servicio</button>
                </div>
            </div>
            <div class="div">
                <a href="descripcion_5.html" class="a_img">
                    <img src="https://ego11.s3.amazonaws.com/products-2020-06/1-23134634.jpg" alt=""/>
                </a>
                <a href="descripcion_5.html">
                    <p>Camara Web Webcam Seenda YM003 Hd 720p Usb Micrófono</p>
                </a>
                <span class="span">Martin Gutierrez</span>
                <div>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <span>0</span>
                    <p>0 reseñas</p>
                </div>
                <p>S/. 120 por hora</p>
                <div class="header">
                    <a href="descripcion_5.html">Más información</a>
                    <button><i class="fa fa-whatsapp"></i> Pedir servicio</button>
                </div>
            </div>
            <div class="div">
                <a href="descripcion_6.html" class="a_img">
                    <img src="https://ego11.s3.amazonaws.com/products-2020-06/68864777_3002040846535584_1907248938675601408_o-2122546.jpg" alt=""/>
                </a>
                <a href="descripcion_6.html">
                    <p>Videos verticales publicitarios para promocionar productos</p>
                </a>
                <span class="span">Anthony Ramirez Zuzunaga</span>
                <div>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/estrellas/01_Iconos-19.png" alt=""/>
                    <span>0</span>
                    <p>0 reseñas</p>
                </div>
                <p>S/. 99 por hora</p>
                <div class="header">
                    <a href="descripcion_6.html">Más información</a>
                    <button><i class="fa fa-whatsapp"></i> Pedir servicio</button>
                </div>
            </div>
            </div>
        </main>
        <article>
            <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/01_Imagen_Hormiga-07.png" alt=""/>
            <div>
                <h1>Fortalece tu negocio</h1>
                <p id="p1">Consigue recomendaciones y gana clientes</p>
                <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/Icono-04.png" alt=""/>
                <h2>Aumenta tus ingresos </h2>
                <p class="psub">Publica tu servicio gratis y llega a mas clientes</p>
                <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/Icono-05.png" alt=""/>
                <h2>Gana libertad</h2>
                <p class="psub">Trabaja a tu ritmo y elige horarios</p>
                <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/Icono-06.png" alt=""/>
                <h2>Entrénate</h2>
                <p class="psub">Conecta como emprendedores como tú en diversos eventos de capacitación</p>
            </div>
        </article>
        <h1 id="ego">Ego, tu comunidad de servicios</h1>
        <div class="d4">
          <Slideshow_2/>   
        </div>
        <div class="d6">Conoce mas sobre nosotros
            <img src="https://ego11.s3-sa-east-1.amazonaws.com/images/home/01_Iconos-10.png" alt=""/>
        </div>
        <div class="d7">
            <div class="d7_" id="d7_1">
                <div class="d7__" id="d7__1">
                    <p id="p1">PARA EMPRENDEDORES</p>
                    <h1>Fortalece tu negocio</h1>
                    <p id="p2">Consigue recomendaciones y gana clientes</p>
                    <button>Enterate como hacerlo</button>
                </div>
            </div>
            <div class="d7_" id="d7_2">
                <div class="d7__" id="d7__2">
                    <p >PARA CLIENTES</p>
                    <h1>Busca el servicio que mejor vaya contigo y recomiendalo</h1>
                    <button>Explorar servicios</button>
                </div>
            </div>
        </div>
        <footer>
            <div class="fp">
                <div class="fp_">
                    <h1>COMUNIDAD</h1> 
                    <ul>
                    <li>Blog</li>
                    <li>eventos</li>
                    </ul>   
                </div>
                <div class="fp_">
                    <h1>CATEGORÍAS</h1>
                    <ul>
                    <li>Profesionales</li>
                    <li>Reparacaciones</li>
                    <li>Enseñanza</li>
                    <li>Servicios Médicos</li>
                    <li>Otros servicios</li>
                    <li>Hogar</li> </ul>
                </div >
                <div class="fp_">
                    <h1>SÍGUENOS</h1>
                    <div class="iconos">
                        <i class="fa fa-facebook-official"></i>
                        <i class="fa fa-twitter"></i>
                        <i class="fa fa-instagram"></i>
                        <i class="fa fa-youtube-play"></i>
                    </div>
                    <br/>
                    <h1>¿NECESITAS AYUDA?</h1>
                    <p><i class="fa fa-question-circle"></i> Preguntas frecuentes</p>
                    <p><i class="fa fa-envelope-o"></i> hola@ego.com</p>
                </div>
            </div>
            <div class="fs">
                <div class="fs_" style={{font_family: "Gilroy-Light"}}>Copyright © Ego 2020 -Todos los derechos reservados</div>
                <nav class="fs_">
                    <p>Sobre ego</p>
                    <p>Términos y condiciones</p>
                    <p>Politicas de privacidad</p>
                </nav>
            </div>
        </footer>  
    </div>
  );
}

export default App;
